# Adminweb de Wambo

Para poder visualizar la web ejecutar los siguientes comandos

```
npm install
```

En una terminal ejecutar el proceso de compilado

```
npm run webpack:dev
```

En otra terminal el servidor de prueba (Este servidor no hace server side rendering)

```
npm run webpack-server
```

Abrir navegador en http://localhos:3000/adminweb

