import 'babel-polyfill';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import IntApp from './modules/Intl/IntApp';
import store from './store';
import './common.css';

ReactDOM.render(
  <Provider store={store}>
    <IntApp />
  </Provider>,
  document.getElementById('root'),
);
