import { combineReducers } from 'redux';
import intlLocale from './modules/Intl/reducer';
import sidenav from './modules/SideNav/reducer';
import postlist from './modules/PostList/reducer';
import detailpost from './modules/DetailPost/reducer';
import newpost from './modules/NewPost/reducer';
import userdetail from './modules/UserDetail/reducer';

const reducer = combineReducers({
  intlLocale,
  sidenav,
  postlist,
  detailpost,
  newpost,
  userdetail,
});

export default reducer;
