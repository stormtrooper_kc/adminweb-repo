import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';
import { fetchPosts } from './actions';
import styles from './PostList.css';


const formatView = (array) => {
  let copy = [].concat(array);
  if (array.length % 3 !== 0) {
    copy = formatView(copy.concat({ none: true }));
    return copy;
  }
  return copy;
};

const formatID = str => (
  str.split('/')[5]
);

@connect(store => (
  {
    items: store.postlist.items,
  }
))
class PostList extends Component {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    this.props.dispatch(fetchPosts());
  }

  render() {
    const itemsToRender = this.props.items;
    const { intl } = this.props;
    let items;
    if (itemsToRender === 'loading') {
      return (
        <div className={styles.loading}>
          <i className="fa fa-spinner fa-pulse fa-3x fa-fw" />
        </div>
      );
    }
    const formatItems = formatView(itemsToRender);
    if (itemsToRender && itemsToRender.length !== 0) {
      items = formatItems.map((obj, index) => {
        let item;
        if (obj.none) {
          item = <div className={styles.carItem} key={`${index}-null`} />;
        } else {
          item = (
            <div className={styles.carItem} key={obj.url}>
              <div className={styles.card}>
                <div className={styles.headerCard}>
                  <h2>{obj.title}</h2>
                  <Link to={`/adminweb/post/${formatID(obj.url)}`}><i className="fa fa-pencil" /></Link>
                </div>
                <div className={styles.contentCard}>
                  <img src={obj.image} alt={obj.title} />
                </div>
                <div className={styles.contentFooter}>
                  <div>Tags: {obj.tags.length}</div>
                  <div>{moment(obj.publish, 'YYYYMMDD').fromNow()}</div>
                </div>
              </div>
            </div>
          );
        }
        return item;
      });
    } else {
      items = <h3>{intl.formatMessage({ id: 'noitems' })}</h3>;
    }
    return (
      <div>
        <div className={styles.postLit}>
          <h2>{intl.formatMessage({ id: 'mypost' })}</h2>
        </div>
        <div className={styles.listContainer}>
          {items}
        </div>
      </div>
    );
  }
}

PostList.propTypes = {
  items: PropTypes.array,
  intl: intlShape.isRequired,
};

PostList.defaultProps = {
  items: [],
};

export default injectIntl(PostList);
