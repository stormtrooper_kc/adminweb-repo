import { FETCH_POSTS } from './actions';

const initialState = {
  items: [],
};

const reducer = (state = { ...initialState }, action) => {
  switch (action.type) {
    case `${FETCH_POSTS}_PENDING`: {
      return { ...state, items: 'loading' };
    }
    case `${FETCH_POSTS}_FULFILLED`: {
      return { ...state, items: action.payload.data.results };
    }
    case `${FETCH_POSTS}_REJECTED`: {
      return { ...state, items: 'loading' };
    }
    default:
      return state;
  }
};

export default reducer;
