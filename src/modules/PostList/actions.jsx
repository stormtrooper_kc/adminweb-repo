import axios from 'axios';

const FETCH_POSTS = 'FETCH_POSTS';
const fetchPosts = () => (
  {
    type: FETCH_POSTS,
    payload: axios.get('/api/posts/'),
  }
);

export {
  FETCH_POSTS,
  fetchPosts,
};
