import axios from 'axios';

const FETH_POST = 'FETH_POST';
const fetchPost = id => (
  {
    type: FETH_POST,
    payload: axios.get(`/api/posts/${id}`),
  }
);

export {
  FETH_POST,
  fetchPost,
};
