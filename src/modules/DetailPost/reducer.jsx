import { FETH_POST } from './actions';

const initialState = {
  loading: true,
  item: {},
};

const reducer = (state = { ...initialState }, action) => {
  switch (action.type) {
    case `${FETH_POST}_PENDING`: {
      return { ...state, loading: true, item: {} };
    }
    case `${FETH_POST}_FULFILLED`: {
      return { ...state, loading: false, item: action.payload.data };
    }
    case `${FETH_POST}_REJECTED`: {
      return { ...state, loading: false, item: 'error' };
    }
    default:
      return state;
  }
};

export default reducer;
