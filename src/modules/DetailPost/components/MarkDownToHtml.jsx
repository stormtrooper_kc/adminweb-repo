import React from 'react';
import PropTypes from 'prop-types';
import showdown from 'showdown';

const converter = new showdown.Converter();

const formatXSS = (html) => {
  const pathNoXSS = /((<)|\)|;|(<\/))+(script)+(>)+/;
  let formatHTML;
  if (html === undefined) return html;
  formatHTML = html.replace(pathNoXSS, '');
  formatHTML = formatHTML.replace(/javascript:/, '');
  return formatHTML;
};

const MarkDownToHtml = (props) => {
  const text = props.html;
  const html = converter.makeHtml(text);
  const innerHTML = {
    __html: formatXSS(html),
  };
  return (
    <div className={props.className}>
      <div dangerouslySetInnerHTML={innerHTML} />
    </div>
  );
};

MarkDownToHtml.propTypes = {
  html: PropTypes.string.isRequired,
  className: PropTypes.string,
};

MarkDownToHtml.defaultProps = {
  className: 'container-markdown',
};

export default MarkDownToHtml;
