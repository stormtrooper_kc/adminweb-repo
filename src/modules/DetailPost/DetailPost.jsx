import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SimpleMDE from 'react-simplemde-editor';
import moment from 'moment';
import { injectIntl, intlShape } from 'react-intl';
import InputForm from './components/InputForm';
import MarkDownToHtml from './components/MarkDownToHtml';
import { fetchPost } from './actions';
import styles from './DetailPost.css';

@connect(store => (
  {
    loading: store.detailpost.loading,
    item: store.detailpost.item,
  }
))
class DetailPost extends Component {
  constructor() {
    super();
    this.state = {
      html: '',
      allPristine: true,
      pristineText: true,
      isValidText: false,
    };
    this.changeText = this.changeText.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(fetchPost(this.props.routeParams.id));
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.item.content) {
      this.setState({ html: nextProps.item.content });
    }
  }

  changeText(value) {
    if (value.trim() === '' || value === undefined) {
      this.setState({
        html: value,
        pristineText: false,
        isValidText: false,
      });
    } else {
      this.setState({
        html: value,
        pristineText: false,
        isValidText: true,
      });
    }
  }

  handleSubmit(ev) {
    ev.preventDefault();
    this.setState({ html: '', allPristine: false });
    const formPost = new FormData(ev.target);
    const jsonToSend = {
      title: formPost.get('title'),
      content: this.state.html,
      publish: new Date(),
      draft: (formPost.get('draft') !== null),
      tags: formPost.get('tags').split(','),
      image: this.state.files[0],
    };
    const dataTag = JSON.stringify(formPost.get('tags').split(','));
    formPost.append('image', this.state.files[0]);
    formPost.append('content', this.state.html);
    formPost.append('draft', (formPost.get('draft') !== null));
    formPost.append('publish', moment(new Date()).format('YYYY-MM-DDThh:mm'));
    formPost.append('tags', dataTag);
    const isValidLength = Object.keys(jsonToSend).filter(obj => (
      jsonToSend[obj] !== undefined
    )).length;
    if (isValidLength === 6) {
      console.log('is valid form');
    }
  }

  render() {
    const text = this.state.html;
    const { item, loading, intl } = this.props;
    if (loading) {
      return (
        <div className={styles.loading}>
          <i className="fa fa-spinner fa-pulse fa-3x fa-fw" />
        </div>
      );
    }
    return (
      <div className={styles.newPost}>
        <div>
          <h2>{intl.formatMessage({ id: 'detailpost' })}</h2>
        </div>
        <div className={styles.postContainer}>
          <div className={styles.itemPost}>
            <div className={styles.card}>
              <form onSubmit={this.handleSubmit} noValidate>
                <InputForm
                  id="titlePost"
                  required
                  name="title"
                  type="text"
                  titleLabel={intl.formatMessage({ id: 'formpostinputtitle' })}
                  errorText="required field"
                  pristine={this.state.allPristine}
                  value={item.title}
                />
                <InputForm
                  id="tags"
                  required
                  name="tags"
                  type="text"
                  titleLabel={intl.formatMessage({ id: 'formposttags' })}
                  errorText="required field"
                  pristine={this.state.allPristine}
                  value={item.tags.join(',')}
                />
                <SimpleMDE
                  onChange={this.changeText}
                  value={this.state.html}
                />
                <div className="checkbox">
                  <label htmlFor="draftCheck">
                    <input type="checkbox" name="draft" required id="draftCheck" value="" />
                    {intl.formatMessage({ id: 'formpostdraft' })}
                  </label>
                </div>
                <button type="submit" className="btn btn-primary">{intl.formatMessage({ id: 'submit' })}</button>
              </form>
            </div>
          </div>
          <div className={styles.itemPost}>
            <div className={styles.card}>
              <div>
                <h3>{intl.formatMessage({ id: 'previewcontent' })}</h3>
              </div>
              <MarkDownToHtml
                html={text}
                className="markdown-air"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

DetailPost.propTypes = {
  loading: PropTypes.bool,
  item: PropTypes.object,
  routeParams: PropTypes.object,
  intl: intlShape.isRequired,
};

DetailPost.defaultProps = {
  loading: true,
  item: {},
  routeParams: {},
};

export default injectIntl(DetailPost);
