import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, IndexLink } from 'react-router';
import { FormattedMessage } from 'react-intl';
import styles from './Layout.css';
import SideNav from '../../SideNav/SideNav';
import { toggleMenu } from '../../SideNav/actions';
import { changeLanguague } from '../../Intl/actions';

@connect(store => (
  {
    languague: store.intlLocale.languague,
  }
))
class Layout extends Component {
  constructor() {
    super();
    this.state = {};
    this.clickMenu = this.clickMenu.bind(this);
    this.clickEN = this.clickEN.bind(this);
    this.clickES = this.clickES.bind(this);
  }

  clickEN() {
    console.log('clickEN');
    this.props.dispatch(changeLanguague('en'));
  }

  clickES() {
    this.props.dispatch(changeLanguague('es'));
  }

  clickMenu() {
    this.props.dispatch(toggleMenu());
  }

  render() {
    const { languague } = this.props;
    let activeEN;
    let activeES;
    if (languague === 'en') {
      activeEN = 'active';
      activeES = '';
    } else {
      activeEN = '';
      activeES = 'active';
    }
    return (
      <div>
        <nav className={styles.navContainer}>
          <div className={styles.itemBrand}>
            <h2>Wambo</h2>
          </div>
          <div onClick={this.clickMenu} className={`${styles.itemBrand} ${styles.itemMenu}`}>
            <div><i className="fa fa-bars" /></div>
          </div>
        </nav>
        <SideNav>
          <ul>
            <IndexLink onClick={this.clickMenu} to="/adminweb/" activeClassName={styles.active}>
              <li className={styles.listItem}>
                <FormattedMessage id="menupostlist" />
              </li>
            </IndexLink>
            <Link onClick={this.clickMenu} to="/adminweb/profile" activeClassName={styles.active}>
              <li className={styles.listItem}>
                <FormattedMessage id="menuprofile" />
              </li>
            </Link>
            <Link onClick={this.clickMenu} to="/adminweb/newPost" activeClassName={styles.active}>
              <li className={styles.listItem}>
                <FormattedMessage id="menunewpost" />
              </li>
            </Link>
            <div className={styles.languague}>
              <button
                onClick={this.clickEN}
                type="button"
                className={`btn btn-default btn-lg ${activeEN}`}
              >
                <FormattedMessage id="english" />
              </button>
              <button
                onClick={this.clickES}
                type="button"
                className={`btn btn-default btn-lg ${activeES}`}
              >
                <FormattedMessage id="spanish" />
              </button>
            </div>
          </ul>
        </SideNav>
        <div>{this.props.children}</div>
      </div>
    );
  }
}

Layout.propTypes = {
  children: PropTypes.any.isRequired,
  languague: PropTypes.string,
};

Layout.defaultProps = {
  languague: 'ES',
};

export default Layout;
