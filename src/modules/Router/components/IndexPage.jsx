import React from 'react';
import PostList from '../../PostList/PostList';

const IndexPage = () => (
  <PostList />
);

export default IndexPage;
