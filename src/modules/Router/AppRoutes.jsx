import React from 'react';
import { Router, browserHistory, Route, IndexRoute } from 'react-router';
import Layout from './components/Layout';
import NotFoundPage from './components/NotFoundPage';
import ForbiddenPage from './components/ForbiddenPage';
import IndexPage from './components/IndexPage';
import NewPost from './../NewPost/NewPost';
import DetailPost from './../DetailPost/DetailPost';
import UserDetail from './../UserDetail/UserDetail';

const routes = (
  <div>
    <Route path="/adminweb" component={Layout}>
      <IndexRoute component={IndexPage} />
      <Route path="/adminweb/newPost" component={NewPost} />
      <Route path="/adminweb/post/:id" component={DetailPost} />
      <Route path="/adminweb/profile" component={UserDetail} />
    </Route>
    <Route path="/403" component={ForbiddenPage} />
    <Route path="/*" component={NotFoundPage} />
  </div>
);

const AppRoutes = () => (
  <Router
    history={browserHistory}
    onUpdate={() => window.scrollTo(0, 0)}
  >
    {routes}
  </Router>
);

export { AppRoutes, routes };
