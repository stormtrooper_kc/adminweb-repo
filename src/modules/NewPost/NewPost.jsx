import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import moment from 'moment';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';
import SimpleMDE from 'react-simplemde-editor';
import InputForm from './components/InputForm';
import MarkDownToHtml from './components/MarkDownToHtml';
import { newPost } from './actions';
import styles from './NewPost.css';

@connect(store => (
  {
    loading: store.newpost.loading,
    msg: store.newpost.msg,
  }
))
class NewPost extends Component {
  constructor() {
    super();
    this.state = {
      html: '',
      allPristine: true,
      pristineText: true,
      isValidText: false,
      files: [],
    };
    this.changeText = this.changeText.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onDrop = this.onDrop.bind(this);
  }

  onDrop(files) {
    console.log(files);
    this.setState({
      files,
    });
  }

  changeText(value) {
    if (value.trim() === '' || value === undefined) {
      this.setState({
        html: value,
        pristineText: false,
        isValidText: false,
      });
    } else {
      this.setState({
        html: value,
        pristineText: false,
        isValidText: true,
      });
    }
  }

  handleSubmit(ev) {
    ev.preventDefault();
    this.setState({ html: '', allPristine: false });
    const formPost = new FormData(ev.target);
    const jsonToSend = {
      title: formPost.get('title'),
      content: this.state.html,
      publish: new Date(),
      draft: (formPost.get('draft') !== null),
      tags: formPost.get('tags').split(','),
      image: this.state.files[0],
    };
    const dataTag = JSON.stringify(formPost.get('tags').split(','));
    console.log(dataTag);
    formPost.append('image', this.state.files[0]);
    formPost.append('content', this.state.html);
    formPost.append('draft', (formPost.get('draft') !== null));
    formPost.append('publish', moment(new Date()).format('YYYY-MM-DDThh:mm'));
    formPost.append('tags', dataTag);
    const isValidLength = Object.keys(jsonToSend).filter(obj => (
      jsonToSend[obj] !== undefined
    )).length;
    if (isValidLength === 6) {
      this.props.dispatch(newPost(formPost));
    }
    console.log(jsonToSend);
  }

  render() {
    const text = this.state.html;
    const { loading, msg, intl } = this.props;
    return (
      <div className={styles.newPost}>
        <div>
          <h2>{intl.formatMessage({ id: 'newposttitle' })}</h2>
        </div>
        <div className={styles.postContainer}>
          <div className={styles.itemPost}>
            <div className={styles.card}>
              <form onSubmit={this.handleSubmit} noValidate>
                <InputForm
                  id="titlePost"
                  required
                  name="title"
                  placeholder={intl.formatMessage({ id: 'formpostinputtitle' })}
                  type="text"
                  titleLabel={intl.formatMessage({ id: 'formpostinputtitle' })}
                  errorText="required field"
                  pristine={this.state.allPristine}
                />
                <InputForm
                  id="publish"
                  required
                  name="tags"
                  placeholder={intl.formatMessage({ id: 'formposttags' })}
                  type="text"
                  titleLabel={intl.formatMessage({ id: 'formposttags' })}
                  errorText="required field"
                  pristine={this.state.allPristine}
                />
                <SimpleMDE
                  onChange={this.changeText}
                />
                <div className="checkbox">
                  <label htmlFor="draftCheck">
                    <input type="checkbox" name="draft" required id="draftCheck" value="" />
                    {intl.formatMessage({ id: 'formpostdraft' })}
                  </label>
                </div>
                <section>
                  <div className={styles.dropzone}>
                    <Dropzone onDrop={this.onDrop}>
                      <p>{intl.formatMessage({ id: 'droptext' })}</p>
                    </Dropzone>
                    <aside>
                      <h4>{intl.formatMessage({ id: 'attachfiles' })}</h4>
                      <ul>
                        {
                          this.state.files.map(f => <li key={f.size}>{f.name} - {f.size} bytes</li>)
                        }
                      </ul>
                    </aside>
                  </div>
                </section>
                {!loading && (
                  <button type="submit" className="btn btn-primary">{intl.formatMessage({ id: 'submit' })}</button>
                )}
                {loading && (
                  <i className="fa fa-spinner fa-pulse fa-3x" />
                )}
                <span>{msg}</span>
              </form>
            </div>
          </div>
          <div className={styles.itemPost}>
            <div className={styles.card}>
              <div>
                <h3>{intl.formatMessage({ id: 'previewcontent' })}</h3>
              </div>
              <MarkDownToHtml
                html={text}
                className="markdown-air"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

NewPost.propTypes = {
  loading: PropTypes.bool,
  msg: PropTypes.string,
  intl: intlShape.isRequired,
};

NewPost.defaultProps = {
  loading: false,
  msg: '',
};

export default injectIntl(NewPost);
