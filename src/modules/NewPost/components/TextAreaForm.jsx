import React, { Component } from 'react';
import PropTypes from 'prop-types';
import autosize from 'autosize';

class TextAreaForm extends Component {
  constructor(props) {
    super();
    this.state = {
      pristine: props.pristineText,
      isValid: props.isValid,
    };
  }

  componentDidMount() {
    autosize(document.getElementById(this.props.id));
  }

  render() {
    const { pristine, isValid } = this.props;
    const allPristine = this.props.pristineText;
    let inputContent;
    if (this.props.required) {
      if (!isValid && (!pristine || !allPristine)) {
        inputContent = (
          <div className="form-group has-error">
            <label htmlFor={this.props.id}>{this.props.titleLabel}</label>
            <textarea
              className="form-control"
              required={this.props.required}
              rows={this.props.rows}
              cols={this.props.cols}
              id={this.props.id}
              name={this.props.name}
              placeholder={this.props.placeholder}
              onChange={this.props.changeText}
            />
            <span className="help-block">
              {this.props.errorText}
            </span>
          </div>
        );
      } else {
        inputContent = (
          <div className="form-group">
            <label htmlFor={this.props.id}>{this.props.titleLabel}</label>
            <textarea
              className="form-control"
              required={this.props.required}
              rows={this.props.rows}
              cols={this.props.cols}
              id={this.props.id}
              name={this.props.name}
              placeholder={this.props.placeholder}
              onChange={this.props.changeText}
            />
          </div>
        );
      }
    } else {
      inputContent = (
        <div className="form-group">
          <label htmlFor={this.props.id}>{this.props.titleLabel}</label>
          <textarea
            className="form-control"
            required={this.props.required}
            rows={this.props.rows}
            cols={this.props.cols}
            id={this.props.id}
            name={this.props.name}
            placeholder={this.props.placeholder}
            onChange={this.props.changeText}
          />
        </div>
      );
    }
    return inputContent;
  }
}

TextAreaForm.propTypes = {
  id: PropTypes.string.isRequired,
  required: PropTypes.bool,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  titleLabel: PropTypes.string,
  errorText: PropTypes.string,
  pristine: PropTypes.bool,
  isValid: PropTypes.bool,
  pristineText: PropTypes.bool,
  changeText: PropTypes.func.isRequired,
  cols: PropTypes.string,
  rows: PropTypes.string,
};

TextAreaForm.defaultProps = {
  required: false,
  placeholder: '',
  errorText: '',
  titleLabel: '',
  pristine: true,
  isValid: false,
  pristineText: true,
  cols: '50',
  rows: '4',
};

export default TextAreaForm;
