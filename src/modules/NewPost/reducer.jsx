import { NEW_POST } from './actions';

const initialState = {
  loading: false,
  error: '',
};

const reducer = (state = { ...initialState }, action) => {
  switch (action.type) {
    case `${NEW_POST}_PENDING`: {
      return { ...state, loading: true, msg: '' };
    }
    case `${NEW_POST}_FULFILLED`: {
      return { ...state, loading: false, msg: 'SUCCESS' };
    }
    case `${NEW_POST}_REJECTED`: {
      return { ...state, loading: false, msg: 'ERROR' };
    }
    default:
      return state;
  }
};

export default reducer;
