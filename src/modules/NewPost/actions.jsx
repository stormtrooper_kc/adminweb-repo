import axios from 'axios';
import getCookie from './../formHelper';

const NEW_POST = 'NEW_POST';
const newPost = (data) => {
  const csrftoken = getCookie('csrftoken');
  return {
    type: NEW_POST,
    payload: axios.post('/api/posts/create/', data, {
      headers: {
        'X-CSRFToken': csrftoken,
      },
    }),
  };
};

export {
  newPost,
  NEW_POST,
};
