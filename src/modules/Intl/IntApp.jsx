import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { addLocaleData, IntlProvider } from 'react-intl';
import { connect } from 'react-redux';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';
import { AppRoutes } from './../Router/AppRoutes';
import messages from './messages.json';

addLocaleData([...en, ...es]);

@connect(store => (
  {
    locale: store.intlLocale.languague,
  }
))
class IntlApp extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <IntlProvider locale={this.props.locale} messages={messages[this.props.locale]}>
        <AppRoutes />
      </IntlProvider>
    );
  }
}

IntlApp.propTypes = {
  locale: PropTypes.string,
};

IntlApp.defaultProps = {
  locale: 'en',
};

export default IntlApp;
