
const CHANGE_LAGUAGUE_ACTION = 'CHANGE_LAGUAGUE';
const changeLanguague = (languague) => {
  if (typeof (Storage) !== 'undefined') {
    localStorage.setItem('locale', languague);
  } else {
    console.warn('Your browser doesn\'t support localstorage');
  }
  return {
    type: CHANGE_LAGUAGUE_ACTION,
    payload: languague,
  };
};

export {
  CHANGE_LAGUAGUE_ACTION,
  changeLanguague,
};
