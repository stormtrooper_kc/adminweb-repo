import { CHANGE_LAGUAGUE_ACTION } from './actions';

let initialState = {};
if (typeof (Storage) !== 'undefined') {
  const langStorage = localStorage.getItem('locale') || 'es';
  initialState = {
    languague: langStorage,
  };
} else {
  console.warn('Your browser doesn\'t support localstorage');
  initialState = {
    languague: navigator.languages.indexOf('es') >= 0 ? 'es' : 'en',
  };
}

const reducer = (state = { ...initialState }, action) => {
  switch (action.type) {
    case CHANGE_LAGUAGUE_ACTION: {
      return { ...state, languague: action.payload };
    }
    default:
      return state;
  }
};

export default reducer;
