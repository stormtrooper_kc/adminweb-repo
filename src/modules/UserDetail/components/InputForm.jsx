import React, { Component } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';

class InputForm extends Component {
  constructor(props) {
    super();
    this.state = {
      pristine: true,
      isValid: false,
      value: props.value,
    };
    this.handleInput = this.handleInput.bind(this);
  }

  handleInput(ev) {
    const target = ev.target;
    console.log(target);
    if (target.type === 'email') {
      if (validator.isEmail(target.value)) {
        this.setState({
          value: target.value,
          isValid: true,
          pristine: false,
        });
      } else {
        this.setState({
          value: target.value,
          isValid: false,
          pristine: false,
        });
      }
    } else if (target.type === 'text') {
      if (!validator.isEmpty(target.value)) {
        this.setState({
          value: target.value,
          isValid: true,
          pristine: false,
        });
      } else {
        this.setState({
          value: target.value,
          isValid: false,
          pristine: false,
        });
      }
    }
  }

  render() {
    const { pristine, isValid } = this.state;
    const allPristine = this.props.pristine;
    let inputContent;
    if (this.props.required) {
      if (!isValid && (!pristine || !allPristine)) {
        inputContent = (
          <div className="form-group has-error">
            <label htmlFor={this.props.id}>{this.props.titleLabel}</label>
            <input
              required={this.props.required}
              type={this.props.type}
              className="form-control"
              onChange={this.handleInput}
              id={this.props.id}
              name={this.props.name}
              placeholder={this.props.placeholder}
              value={this.state.value}
            />
            <span className="help-block">
              {this.props.errorText}
            </span>
          </div>
        );
      } else {
        inputContent = (
          <div className="form-group">
            <label htmlFor={this.props.id}>{this.props.titleLabel}</label>
            <input
              required={this.props.required}
              type={this.props.type}
              className="form-control"
              onChange={this.handleInput}
              id={this.props.id}
              name={this.props.name}
              placeholder={this.props.placeholder}
              value={this.state.value}
            />
          </div>
        );
      }
    } else {
      inputContent = (
        <div className="form-group">
          <label htmlFor={this.props.id}>{this.props.titleLabel}</label>
          <input
            required={this.props.required}
            type={this.props.type}
            className="form-control"
            onChange={this.handleInput}
            id={this.props.id}
            name={this.props.name}
            placeholder={this.props.placeholder}
            value={this.state.value}
          />
        </div>
      );
    }
    return inputContent;
  }
}

InputForm.propTypes = {
  id: PropTypes.string.isRequired,
  required: PropTypes.bool,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  type: PropTypes.string.isRequired,
  titleLabel: PropTypes.string,
  errorText: PropTypes.string,
  pristine: PropTypes.bool,
  value: PropTypes.string,
};

InputForm.defaultProps = {
  required: false,
  placeholder: '',
  errorText: '',
  titleLabel: '',
  pristine: true,
  value: '',
};

export default InputForm;
