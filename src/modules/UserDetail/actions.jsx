import axios from 'axios';
import getCookie from './../formHelper';


const FETCH_USER = 'FETCH_USER';
const fetchUser = () => {
  const csrftoken = getCookie('csrftoken');
  return {
    type: FETCH_USER,
    payload: axios.get('/api/users/me', {
      headers: {
        'X-CSRFToken': csrftoken,
      },
    }),
  };
};

export {
  fetchUser,
  FETCH_USER,
};
