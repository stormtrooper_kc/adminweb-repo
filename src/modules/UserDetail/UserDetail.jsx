import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';
import InputForm from './components/InputForm';
import TextAreaForm from './components/TextAreaForm';
import { fetchUser } from './actions';
import styles from './UserDetail.css';

@connect(store => (
  {
    loading: store.userdetail.loading,
    item: store.userdetail.item,
  }
))
class UserDetail extends Component {
  constructor() {
    super();
    this.state = {
      allPristine: true,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(fetchUser());
  }

  handleSubmit(ev) {
    ev.preventDefault();
    this.setState({ html: '', allPristine: false });
    const formPost = new FormData(ev.target);
    const jsonToSend = {
      username: formPost.get('username'),
      firstName: formPost.get('firstName'),
      lastName: formPost.get('lastName'),
      email: formPost.get('email'),
    };
    console.log(jsonToSend);
  }

  render() {
    const { item, loading, intl } = this.props;
    if (loading) {
      return (
        <div className={styles.loading}>
          <i className="fa fa-spinner fa-pulse fa-3x fa-fw" />
        </div>
      );
    }
    return (
      <div>
        <div className={styles.cardHeader}>
          <h2>{intl.formatMessage({ id: 'youraccount' })}</h2>
        </div>
        <div className={styles.card}>
          <div className={styles.contentDetail}>
            <div className={styles.contentImage}>
              <img src={item.profile.photo} alt={item.username} />
            </div>
            <form onSubmit={this.handleSubmit} noValidate>
              <InputForm
                id="username"
                required
                name="username"
                type="text"
                titleLabel={intl.formatMessage({ id: 'formusername' })}
                errorText="required field"
                pristine={this.state.allPristine}
                value={item.username}
              />
              <InputForm
                id="firstName"
                required
                name="firstName"
                type="text"
                titleLabel={intl.formatMessage({ id: 'formfirstname' })}
                errorText="required field"
                pristine={this.state.allPristine}
                value={item.first_name}
              />
              <InputForm
                id="lastName"
                required
                name="lastName"
                type="text"
                titleLabel={intl.formatMessage({ id: 'lastname' })}
                errorText="required field"
                pristine={this.state.allPristine}
                value={item.last_name}
              />
              <InputForm
                id="email"
                required
                name="email"
                type="email"
                titleLabel={intl.formatMessage({ id: 'email' })}
                errorText="required field"
                pristine={this.state.allPristine}
                value={item.email}
              />
              <TextAreaForm
                id="bio"
                required
                name="bio"
                titleLabel={intl.formatMessage({ id: 'bio' })}
                value={item.profile.bio}
                errorText="required field"
                pristine={this.state.allPristine}
              />
              <button type="submit" className="btn btn-primary">Update Account</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

UserDetail.propTypes = {
  loading: PropTypes.bool,
  item: PropTypes.object,
  intl: intlShape.isRequired,
};

UserDetail.defaultProps = {
  loading: true,
  item: {},
};

export default injectIntl(UserDetail);
