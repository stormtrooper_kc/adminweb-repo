import { FETCH_USER } from './actions';

const initialState = {
  loading: true,
  item: {},
};

const reducer = (state = { ...initialState }, action) => {
  switch (action.type) {
    case `${FETCH_USER}_PENDING`: {
      return { ...state, loading: true, item: {} };
    }
    case `${FETCH_USER}_FULFILLED`: {
      return { ...state, loading: false, item: action.payload.data };
    }
    case `${FETCH_USER}_REJECTED`: {
      return { ...state, loading: false, item: 'error' };
    }
    default:
      return state;
  }
};

export default reducer;
