import { TOGGLE_MENU } from './actions';

const initialState = {
  open: false,
};

const reducer = (state = { ...initialState }, action) => {
  switch (action.type) {
    case TOGGLE_MENU: {
      if (!state.open) {
        document.body.style.overflow = 'hidden';
      } else {
        document.body.style.overflow = 'visible';
      }
      return { ...state, open: !state.open };
    }
    default:
      return state;
  }
};

export default reducer;
