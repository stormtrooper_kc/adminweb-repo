import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styles from './SideNav.css';
import { toggleMenu } from './actions';


@connect(store => (
  {
    open: store.sidenav.open,
  }
))
class SideNav extends Component {
  constructor() {
    super();
    this.state = {};
    this.clickBackDrop = this.clickBackDrop.bind(this);
  }

  clickBackDrop() {
    this.props.dispatch(toggleMenu());
  }

  render() {
    let styleSideNav;
    let backClassName;
    if (this.props.open) {
      backClassName = styles.backdrop;
      styleSideNav = styles.sideNavContentOpen;
    } else {
      backClassName = styles.none;
      styleSideNav = `${styles.sideNavContentOpen} ${styles.close}`;
    }
    return (
      <div>
        <div className={styleSideNav}>
          <div className={styles.headerBar}>
            {this.props.title}
          </div>
          <div className={styles.listContainer}>
            {this.props.children}
          </div>
        </div>
        <div onClick={this.clickBackDrop} className={backClassName} />
      </div>
    );
  }
}

SideNav.propTypes = {
  title: PropTypes.string,
  open: PropTypes.bool,
};

SideNav.defaultProps = {
  title: 'title',
  open: false,
};

export default SideNav;
