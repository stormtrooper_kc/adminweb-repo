
let postCssPlugins = [];

if (process.env.NODE_ENV === 'production') {
  postCssPlugins = [
    require('autoprefixer'),
    require('cssnano')({ zindex: false }),
  ];
}
else {
  postCssPlugins = [
    require('autoprefixer'),
  ];
}


module.exports = {
  plugins: postCssPlugins,
};
