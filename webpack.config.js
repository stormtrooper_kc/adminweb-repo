const webpack = require('webpack');
const path = require('path');

const configClient = {
  devServer: {
    contentBase: path.join(__dirname, 'build'),
    compress: true,
    port: 3000,
    watchOptions: {
      poll: true,
    },
  },
  devtool: 'source-map',
  entry: path.join(__dirname, 'src', 'app-client.jsx'),
  output: {
    path: path.join(__dirname, 'build', 'js'),
    filename: 'bundle.js',
  },
  module: {
    loaders: [
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: {
          babelrc: false,
          presets: ['es2015', 'es2016', 'es2017', 'stage-2', 'react'],
          plugins: ['transform-es2015-modules-commonjs', 'transform-decorators-legacy'],
        },
      },
      {
        test: /\.css$/,
        loaders: [
          'style-loader?sourceMap',
          'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
          'postcss-loader',
        ],
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      },
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
  ],
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  target: 'web',
};

if (process.env.NODE_ENV === 'production') {
  const productionPluggins = [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: false,
      mangle: false,
    }),
  ];
  configClient.output = {
    path: path.join(__dirname, '../wambo', 'static', 'adminweb', 'js'),
    filename: 'bundle.js',
  };
  configClient.plugins = productionPluggins;
}

module.exports = configClient;
